#ifndef __STATEDEF__
#define __STATEDEF__

#include "StateImp.h"


#define NO_CHANGE_MASK      -1
#define STANDBY_MASK        0
#define ON_MASK             1
#define _ON_MASK            2
#define __ON_MASK           3
#define READY_MASK          4
#define _READY_MASK         5
#define MAKE_MASK           6
#define COFFEE_MASK         7
#define _COFFEE_MASK        8
#define MAINTENANCE_MASK    9


class Standby: public StateImp {
public:
    Standby(int16_t *num_of_coffee);
    ~Standby();
    int16_t check_state();
};


class On: public StateImp {
public:
    On(int16_t *num_of_coffee);
    int16_t check_state();
};


class _On: public StateImp {
public:
    _On(int16_t *num_of_coffee);
    ~_On();
    int16_t check_state();
};


class __On: public StateImp {
public:
    __On(int16_t *num_of_coffee);
    ~__On();
    int16_t check_state();
};


class Ready: public StateImp {
public:
    Ready(int16_t *num_of_coffee);
    int16_t check_state();
};


class _Ready: public StateImp {
public:
    _Ready(int16_t *num_of_coffee);
    ~_Ready();
    int16_t check_state();
};


class Make: public StateImp {
public:
    Make(int16_t *num_of_coffee);
    ~Make();
    int16_t check_state();
};


class Coffee: public StateImp {
public:
    Coffee(int16_t *num_of_coffee);
    ~Coffee();
    int16_t check_state();
};


class _Coffee: public StateImp {
public:
    _Coffee(int16_t *num_of_coffee);
    int16_t check_state();
};


class Maintenance: public StateImp {
public:
    Maintenance(int16_t *num_of_coffee);
    int16_t check_state();
};


#endif
