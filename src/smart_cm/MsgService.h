#ifndef __MSGSERVICE__
#define __MSGSERVICE__

#include "Arduino.h"

class Msg {
    String content;

public:
    Msg(String content){
      this->content = content;
    }
    
    String getContent(){
      return this->content;
    }
  
    int16_t getRefill() {
        if (this->content.substring(0,9) == "TT-Refill") {
            return 1;
        } else {
          return 0;
        }
    }
};

class Pattern {
public:
  virtual boolean match(const Msg& m) = 0;  
};

class MsgServiceClass {
    
public: 
  
  Msg* currentMsg;
  bool msgAvailable;

  void init();  

  bool isMsgAvailable();
  Msg* receiveMsg();

  bool isMsgAvailable(Pattern& pattern);
  Msg* receiveMsg(Pattern& pattern);
  
  //void sendMsg(const String& msg);
  void sendMsg(char *buff);
  void sendCtl(char *buff);
  void sendPot(int16_t val);
};

extern MsgServiceClass MsgService;

#endif
