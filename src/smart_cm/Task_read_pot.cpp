#include "Task_read_pot.h"
#include "MsgService.h"


Task_read_pot::Task_read_pot (int16_t pin, StateImp **state_pp){
    //locked = false;
    this->pin = pin;
    this->state_pp = state_pp;
    this->pot = new PotImp(POT_PIN);
    //this->pot = new PotFake();
}

Task_read_pot::~Task_read_pot (){
    delete this->pot;
}

void Task_read_pot::run() {
    (*this->state_pp)->update_pot_val(this->pot->get_value());
    MsgService.sendPot(this->pot->get_value());
}
