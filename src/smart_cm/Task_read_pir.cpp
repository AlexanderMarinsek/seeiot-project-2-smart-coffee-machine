#include "Task_read_pir.h"


Task_read_pir::Task_read_pir (int16_t pin, StateImp **state_pp){
    //locked = false;
    this->pin = pin;
    this->state_pp = state_pp;
    this->pir = new PirImp(PIR_PIN);
    //this->pir = new PirFake();
}

Task_read_pir::~Task_read_pir (){
    delete this->pir;
}

void Task_read_pir::run() {
    (*this->state_pp)->update_pir_val(this->pir->get_movement());
}
