#include "Task.h"
#include "Arduino.h"

void Task::calc_next() {
    unsigned long now = micros();
    this->next_period = now + this->period;
    this->next_deadline = this->next_period + this->deadline;
}

bool Task::is_locked() {
    return this->locked;
}
