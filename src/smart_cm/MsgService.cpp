#include "Arduino.h"
#include "MsgService.h"

String content;

MsgServiceClass MsgService;

bool MsgServiceClass::isMsgAvailable(){
  return msgAvailable;
}

Msg* MsgServiceClass::receiveMsg(){
  if (msgAvailable){
    Msg* msg = currentMsg;
    msgAvailable = false;
    currentMsg = NULL;
    content = "";
    return msg;  
  } else {
    return NULL; 
  }
}

void MsgServiceClass::init(){
  Serial.begin(9600);
  content.reserve(256);
  content = "";
  currentMsg = NULL;
  msgAvailable = false;  
}

//void MsgServiceClass::sendMsg(const String &msg){
void MsgServiceClass::sendMsg(char *buff){
  //Serial.println(msg);  
  Serial.println(buff);  
}

void MsgServiceClass::sendCtl(char *buff){
  char _buff [32];
  sprintf(_buff, "TT-%s", buff);
  //Serial.println(_buff);  
  sendMsg(_buff);
}

void MsgServiceClass::sendPot(int16_t val){
  char _buff [32];
  sprintf(_buff, "zucchero-%04d", val);
  sendCtl(_buff);
}

void serialEvent() {
  /* reading the content */
  while (Serial.available()) {
    char ch = (char) Serial.read();
    if (ch == '\n'){
      MsgService.currentMsg = new Msg(content);
      MsgService.msgAvailable = true;      
    } else {
      content += ch;      
    }
  }
}

bool MsgServiceClass::isMsgAvailable(Pattern& pattern){
  return (msgAvailable && pattern.match(*currentMsg));
}

Msg* MsgServiceClass::receiveMsg(Pattern& pattern){
  Serial.print("Address: ");
  Serial.println((int)this->currentMsg);
  if (msgAvailable && pattern.match(*currentMsg)){
    Msg* msg = this->currentMsg;
    this->msgAvailable = false;
    //this->currentMsg = NULL;
    delete this->currentMsg;
    content = "";
    return msg;  
  } else {
    return NULL; 
  }
  
}
