#ifndef __TASK__
#define __TASK__

#include "StateImp.h"
#include "Arduino.h"
#include <stdint.h>

class Task {
public: 
    Task(){};
    virtual ~Task(){};
    virtual void run() = 0;
    void calc_next();
    bool is_locked();
protected:
    bool locked = false;
    int8_t priority;
    int32_t period;
    int32_t deadline;
    unsigned long next_period;
    unsigned long next_deadline;
    //StateImp *state;
    StateImp  **state_pp;
};

#endif
