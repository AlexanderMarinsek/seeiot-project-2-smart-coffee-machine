#include "SonarImp.h"
#include "Arduino.h"
#include <stdint.h>


SonarImp::SonarImp (int16_t trigPin, int16_t echoPin) {
    this->trigPin = trigPin;
    this->echoPin = echoPin;
    pinMode(trigPin, OUTPUT);
    pinMode(echoPin, INPUT);
}


int16_t SonarImp::get_distance() {
    unsigned long t1 = micros();
    digitalWrite(this->trigPin,LOW);
    delayMicroseconds(3);
    digitalWrite(this->trigPin,HIGH);
    delayMicroseconds(5);
    digitalWrite(this->trigPin,LOW);
    
    float tUS = pulseIn(this->echoPin, HIGH);
    float t = tUS / 1000.0 / 1000.0 / 2;
    float d = t*_VS;

    if (micros() - t1 > 10000) {
        Serial.print("OBJECT TOO FAR FOR SONAR!!! Time (us): ");
        Serial.println(micros() - t1);
    }

    if (d <= 0.1) {
        return 2;
    } 
    else if (d <= 0.3) {
        return 1;
    }
    else {
        return 0;
    }
}
