#include "PirImp.h"
#include "Arduino.h"
#include <stdint.h>


PirImp::PirImp (int16_t outputPin) {
    this->outputPin = outputPin;
    pinMode(outputPin, INPUT);  
}


int16_t PirImp::get_movement() {
    return digitalRead(this->outputPin);
}
