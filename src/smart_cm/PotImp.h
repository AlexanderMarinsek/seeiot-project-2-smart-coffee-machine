#ifndef __POTIMP__
#define __POTIMP__


#include "Pot.h"
#include "Arduino.h"
#include <stdint.h>


#define AMB_TEMP 20
#define VS (331.5 + 0.6*AMB_TEMP)


class PotFake: public Pot {
public:
    int16_t get_value (){
      return (int16_t)random(0,1023);
    };
};


class PotImp: public Pot {
public:
    PotImp (int16_t outputPin);
    int16_t get_value();
protected:
    int16_t outputPin;
};


#endif
