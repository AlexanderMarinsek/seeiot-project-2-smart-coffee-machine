#ifndef __BUTTONIMPL__
#define __BUTTONIMPL__


#include "Button.h"
#include "Arduino.h"
#include <stdint.h>


#define DEBOUNCE_TIME_MS 50


class ButtonFake: public Button {
public: 
  int16_t get_status() {
    return (int16_t)random(0,2);
  }
};


class ButtonImp: public Button {
public: 
  ButtonImp(int pin);
  int16_t get_status();
protected:
  int16_t pin;
  int16_t last_status = 0;
  unsigned long last_call = 0;
};


#endif
