
#include "StateDef.h"
#include "Task_state_m.h"
#include "Scheduler.h"
#include "Tasks.h"
#include "GeneralConst.h"
#include "Pins.h"

#include <avr/sleep.h>


StateImp *state;

Scheduler *sched = new Scheduler();

// dummy value, gets overwritten in 'state machine task'
int16_t num_of_cof = COFFEE_REFILL;

void wakeUp(){}


void setup() {
    Serial.begin(9600);    

    sched->add_task(new Task_state_m(&state, sched));
    sched->add_task(new Task_read_pir(PIR_PIN, &state));
    //sched->add_task(new Task_read_dist(TRIG_PIN, ECHO_PIN, &state));
    //sched->add_task(new Task_read_pot(POT_PIN, &state));
    //sched->add_task(new Task_read_btn(BTN_PIN, &state));

    state = new Standby(&num_of_cof);
    //state = new Maintenance(&num_of_cof);

    Serial.println("** Init complete **");
    delay(3000);
}


void loop() {

    sched->run();
    
}
