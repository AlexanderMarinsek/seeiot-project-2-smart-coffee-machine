#ifndef __POT__
#define __POT__


#include <stdint.h>


class Pot {
public:
    virtual int16_t get_value();
};


#endif
