#ifndef __TASK_LED__
#define __TASK_LED__

#include "Task.h"
#include "Timer.h"
#include <stdint.h>


class Task_led: public Task {
private:
    int16_t pin1;
    int16_t pin2;
    int16_t pin3;
    int16_t led_status = 1;
    Timer *timer;
public:
    Task_led(int16_t pin1, int16_t pin2, int16_t pin3, StateImp **state_pp);
    ~Task_led ();
    void run();
};


#endif
