#include "Task_read_dist.h"


Task_read_dist::Task_read_dist (int16_t trig_pin, int16_t echo_pin, StateImp **state_pp){
    //locked = false;
    this->pin = pin;
    this->state_pp = state_pp;
    
    this->sonar = new SonarImp(trig_pin, echo_pin);
    //this->sonar = new SonarFake();
}

Task_read_dist::~Task_read_dist (){
    delete this->sonar;
}

void Task_read_dist::run() {
    (*this->state_pp)->update_dist_val(this->sonar->get_distance());
}
