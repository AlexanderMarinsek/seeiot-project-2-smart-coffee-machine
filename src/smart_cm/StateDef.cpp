#include "StateDef.h"
#include "TimerImp.h"
#include "MsgService.h"
#include "Pins.h"
#include "Arduino.h"
#include <avr/sleep.h>


#include "GeneralConst.h"


void wakeUpNow(){ }

Standby::Standby(int16_t *num_of_coffee) {
  this->current_state_mask = STANDBY_MASK;
  this->num_of_coffee = num_of_coffee;
  this->timer = new TimerImp(TIMER_DT1_MS);
}
Standby::~Standby() {
  delete this->timer;
}

int16_t Standby::check_state() {

    // Comment the whole "if statement" when using FAKE peripherals
    if (this->timer->has_finished()) {
        attachInterrupt(digitalPinToInterrupt(WAKEUP_PIN), wakeUpNow, RISING); 
        set_sleep_mode(SLEEP_MODE_PWR_DOWN);
        sleep_enable();
        sleep_mode();
        sleep_disable();
        detachInterrupt(digitalPinToInterrupt(WAKEUP_PIN));
        delete this->timer;
        this->timer = new TimerImp(TIMER_DT1_MS);
    }
    // Put comment ending here
  
    if (this->pir_val > 0) {
      return ON_MASK;
    } else {
      return NO_CHANGE_MASK;
    }
}



On::On(int16_t *num_of_coffee) {
  this->current_state_mask = ON_MASK;
  this->num_of_coffee = num_of_coffee;
}

int16_t On::check_state() {  
  if (this->pir_val == 0) {
    return _ON_MASK;
  }
  // either dist1, or dist2 criteria has to be met
  else if (this->dist_val >= 1) {
    return __ON_MASK;
  }
  else {
    return NO_CHANGE_MASK;
  }
}


_On::_On(int16_t *num_of_coffee) {
  this->num_of_coffee = num_of_coffee;
  this->current_state_mask = _ON_MASK;
  this->timer = new TimerImp(TIMER_DT1_MS);
}
_On::~_On() {
  delete this->timer;
}

int16_t _On::check_state() {
  if (this->pir_val == 1) {
    return ON_MASK;
  }
  else if (this->pir_val == 0 && this->timer->has_finished() == 1) {
    return STANDBY_MASK;
  }
  else {
    return NO_CHANGE_MASK;
  }
}


__On::__On(int16_t *num_of_coffee) {
  //Serial.println("Create __On");
  this->num_of_coffee = num_of_coffee;
  this->current_state_mask = __ON_MASK;
  this->timer = new TimerImp(TIMER_DT2B_MS);
}

__On::~__On() {
  //Serial.println("Destroy __On");
  delete this->timer;
}

int16_t __On::check_state() {
  if (this->dist_val >= 1 && this->timer->has_finished() == 1) {
      //Serial.print("Timer __ON: ");
      //Serial.println(this->timer->has_finished());
      return READY_MASK;
  }
  else if (this->dist_val == 0) {
      return ON_MASK;
  }
  else {
      return NO_CHANGE_MASK;
  }
}


Ready::Ready(int16_t *num_of_coffee) {
    this->num_of_coffee = num_of_coffee;
    this->current_state_mask = READY_MASK;
}

int16_t Ready::check_state() {
    if (this->btn_val == 1) {
        return MAKE_MASK;
    }
    else if (this->dist_val == 0) {
        return _READY_MASK;
    }
    else {
        return NO_CHANGE_MASK;
    }
}


_Ready::_Ready(int16_t *num_of_coffee) {
    this->num_of_coffee = num_of_coffee;
    this->current_state_mask = _READY_MASK;;
    this->timer = new TimerImp(TIMER_DT2A_MS);
}

_Ready::~_Ready() {
  //Serial.println("Destroy __On");
  delete this->timer;
}

int16_t _Ready::check_state() {
    if (this->dist_val >= 1) {
        return READY_MASK;
    }
    else if (this->dist_val == 0 && this->timer->has_finished() == 1) {
        return ON_MASK;
    }
    else {
        return NO_CHANGE_MASK;
    }
}


Make::Make(int16_t *num_of_coffee) {
    this->num_of_coffee = num_of_coffee;
    this->current_state_mask = MAKE_MASK;
    this->timer = new TimerImp(TIMER_DT3_MS);
}

Make::~Make() {
    delete this->timer;
}

int16_t Make::check_state() {
    if (this->timer->has_finished() == 1) {
        return COFFEE_MASK;
    }
    else {
        return NO_CHANGE_MASK;
    }
}


Coffee::Coffee(int16_t *num_of_coffee) {
    this->num_of_coffee = num_of_coffee;
    this->current_state_mask = COFFEE_MASK;
    this->timer = new TimerImp(TIMER_DT4_MS);
}

Coffee::~Coffee() {
    delete this->timer;
}

int16_t Coffee::check_state() {
    if (this->dist_val == 2 || this->timer->has_finished() == 1) {
        return _COFFEE_MASK;
    }
    else {
        return NO_CHANGE_MASK;
    }
}


_Coffee::_Coffee(int16_t *num_of_coffee) {
    this->num_of_coffee = num_of_coffee;
    this->current_state_mask = _COFFEE_MASK;
    *this->num_of_coffee = *this->num_of_coffee - 1;
}

int16_t _Coffee::check_state() {
    if (*this->num_of_coffee == 0) {
        return MAINTENANCE_MASK;
    }
    else {
        return READY_MASK;
    }
}


Maintenance::Maintenance(int16_t *num_of_coffee) {
    this->num_of_coffee = num_of_coffee;
    this->current_state_mask = MAINTENANCE_MASK;
}

int16_t Maintenance::check_state() {
  
    if (MsgService.isMsgAvailable()) {
        //Serial.println("Available");
        //Serial.println(msg->getContent());
        Msg* msg = MsgService.receiveMsg();
        if (msg->getRefill()) {
            *this->num_of_coffee = COFFEE_REFILL;
            //Serial.println("Refilled");
            //Serial.println(*this->num_of_coffee);
        }
    }
  
    if (*this->num_of_coffee == 0) {
        return NO_CHANGE_MASK;
    }
    else {
        return STANDBY_MASK;
    }
}
