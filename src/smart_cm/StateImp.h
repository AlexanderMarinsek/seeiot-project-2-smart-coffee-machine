#ifndef __STATEIMP__
#define __STATEIMP__

#include "State.h"
#include "TimerImp.h"

class StateImp: public State {
public:
    StateImp(){};
    virtual ~StateImp(){};
    void update_pir_val(bool val);
    void update_pot_val(int16_t val);
    void update_dist_val(int16_t val);
    void update_btn_val(int16_t val);
    void update_timer_val(unsigned long val);
    virtual int16_t check_state() = 0;
    //virtual void change_state() = 0;
    int16_t *num_of_coffee;
protected:
    bool pir_val = 0;
    int16_t pot_val = 0;
    int16_t dist_val = 0;
    int16_t btn_val = 0;
    Timer *timer;
};

#endif
