#ifndef __SONARIMP__
#define __SONARIMP__


#include "Sonar.h"
#include "Arduino.h"
#include <stdint.h>


#define AMB_TEMP 20
#define _VS (331.5 + 0.6*AMB_TEMP)


class SonarFake: public Sonar {
public:
    int16_t get_distance() {
        return (int16_t)random(0,3);
    }
};


class SonarImp: public Sonar {
public:
    SonarImp (int16_t trigPin, int16_t echoPin);
    int16_t get_distance();
protected:
    int16_t trigPin;
    int16_t echoPin;
};


#endif
