#ifndef __STATE__
#define __STATE__

#include <stdint.h>

class State {
public:
    State(){};
    virtual ~State(){};
    uint16_t current_state_mask;
    //uint16_t state_a_mask;
    //uint16_t state_b_mask; 
};

#endif
