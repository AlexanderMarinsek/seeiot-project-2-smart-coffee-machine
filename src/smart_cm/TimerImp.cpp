#include "TimerImp.h"
#include "Arduino.h"
#include <stdint.h>


TimerImp::TimerImp(unsigned long period_ms) {
    this->period_ms = period_ms;
    this->start_time = millis();
}


int16_t TimerImp::has_finished() {
    if (millis() - this->start_time >= this->period_ms) {
        return 1;
    } else {
        return 0;
    }
}
