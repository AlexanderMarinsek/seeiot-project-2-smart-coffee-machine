#ifndef __TASK_READ_DIST__
#define __TASK_READ_DIST__

#include "Task.h"
#include "SonarImp.h"
#include "Pins.h"
#include <stdint.h>


class Task_read_dist: public Task {
private:
    int16_t pin;
    Sonar *sonar;
public:
    Task_read_dist(int16_t trig_pin, int16_t echo_pin, StateImp **state_pp);
    ~Task_read_dist ();
    void run();
};


#endif
