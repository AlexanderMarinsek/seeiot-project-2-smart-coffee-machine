#ifndef __PIRIMP__
#define __PIRIMP__


#include "Pir.h"
#include "Arduino.h"
#include <stdint.h>


class PirFake: public Pir {
public:
    int16_t get_movement() {
        return (int16_t)random(0,2);
    }
};


class PirImp: public Pir {
public:
    PirImp (int16_t outputPin);
    int16_t get_movement();
protected:
    int16_t outputPin;
};


#endif
