#include "Task_state_m.h"
#include "Arduino.h"

#include "StateDef.h"
#include "Tasks.h"

#include "MsgService.h"

#include "Pins.h"


int16_t num_of_coffee = INITIAL_NUM_OF_COFFEE;


//Task_state_m::Task_state_m (State* state){
Task_state_m::Task_state_m (StateImp **state_pp, Scheduler *sched_p){
    this->locked = true;
    this->state_pp = state_pp;
    this->sched_p = sched_p;
}

void Task_state_m::run() {
    char buff [32];
  
    //Serial.println("State_m");
    //Serial.println((*this->state_pp)->state_a_mask);
    //Serial.println((*this->state_pp)->state_b_mask); 

    //Serial.print("State current: ");
    //Serial.println((*this->state_pp)->current_state_mask);
    
    int16_t state_change = (*this->state_pp)->check_state();
    //Serial.print("State change: ");
    //Serial.println(state_change);
    
    //Serial.print("Num of Coffees: ");
    //Serial.println(num_of_coffee);

    if (state_change == STANDBY_MASK) {
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_read_pir(PIR_PIN, this->state_pp));
        delete *this->state_pp;     
        *this->state_pp = new Standby(&num_of_coffee);  
    } 
    else if (state_change == ON_MASK) {
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_read_pir(PIR_PIN, this->state_pp));
        this->sched_p->add_task(new Task_read_dist(TRIG_PIN, ECHO_PIN, this->state_pp));
        delete *this->state_pp;     
        *this->state_pp = new On(&num_of_coffee);  
    }
    else if (state_change == _ON_MASK) {
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_read_pir(PIR_PIN, this->state_pp));
        delete *this->state_pp;     
        *this->state_pp = new _On(&num_of_coffee);  
    }
    else if (state_change == __ON_MASK) {
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_read_dist(TRIG_PIN, ECHO_PIN, this->state_pp));
        delete *this->state_pp;     
        *this->state_pp = new __On(&num_of_coffee);  
    }
    else if (state_change == READY_MASK) {
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_read_pot(POT_PIN, this->state_pp));
        this->sched_p->add_task(new Task_read_btn(BTN_PIN, this->state_pp));
        this->sched_p->add_task(new Task_read_dist(TRIG_PIN, ECHO_PIN, this->state_pp));
        delete *this->state_pp;     
        *this->state_pp = new Ready(&num_of_coffee);  
        sprintf(buff, "Welcome");
        MsgService.sendCtl(buff);
    }
    else if (state_change == _READY_MASK) { 
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_read_dist(TRIG_PIN, ECHO_PIN, this->state_pp));
        delete *this->state_pp;     
        *this->state_pp = new _Ready(&num_of_coffee);  
    }
    else if (state_change == MAKE_MASK) { 
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_led(LED_PIN_1, LED_PIN_2, LED_PIN_3, this->state_pp));
        delete *this->state_pp;     
        *this->state_pp = new Make(&num_of_coffee);
        sprintf(buff, "Doing");
        MsgService.sendCtl(buff);
    }
    else if (state_change == COFFEE_MASK) { 
        this->sched_p->clear_tasks();
        this->sched_p->add_task(new Task_read_dist(TRIG_PIN, ECHO_PIN, this->state_pp));
        delete *this->state_pp;     
        *this->state_pp = new Coffee(&num_of_coffee);
    }
    else if (state_change == _COFFEE_MASK) { 
        this->sched_p->clear_tasks();
        delete *this->state_pp;     
        *this->state_pp = new _Coffee(&num_of_coffee);
        if (num_of_coffee) {
            sprintf(buff, "Ready");
            MsgService.sendCtl(buff);
        } else {
            sprintf(buff, "Empty");
            MsgService.sendCtl(buff);
        }
    }
    else if (state_change == MAINTENANCE_MASK) { 
        this->sched_p->clear_tasks();
        delete *this->state_pp;     
        *this->state_pp = new Maintenance(&num_of_coffee);
    }
    else if (state_change == NO_CHANGE_MASK) {
        // nothing
    }
    
}
