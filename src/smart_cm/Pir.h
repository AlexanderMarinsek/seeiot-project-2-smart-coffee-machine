#ifndef __PIR__
#define __PIR__


#include <stdint.h>


class Pir {
public:
    virtual int16_t get_movement();
};


#endif
