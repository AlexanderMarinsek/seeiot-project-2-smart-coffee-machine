#ifndef __TASK_READ_PIR__
#define __TASK_READ_PIR__

#include "Task.h"
#include "StateImp.h"
#include "PirImp.h"
#include "Pins.h"
#include <stdint.h>


class Task_read_pir: public Task {
private:
    int16_t pin;
    Pir *pir;
public:
    Task_read_pir(int16_t pin, StateImp **state_pp);
    ~Task_read_pir ();
    void run();
};


#endif
