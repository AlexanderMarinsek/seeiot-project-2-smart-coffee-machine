#include "ButtonImp.h"
#include "Arduino.h"
#include <stdint.h>


ButtonImp::ButtonImp(int pin){
  this->pin = pin;
  pinMode(pin, INPUT);     
} 
  
int16_t ButtonImp::get_status(){
  if (millis() - this->last_call > DEBOUNCE_TIME_MS) {
      this->last_call = millis();
      this->last_status = (int16_t)digitalRead(pin);
  }
  return last_status;
}
