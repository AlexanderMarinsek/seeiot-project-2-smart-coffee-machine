#ifndef __BUTTON__
#define __BUTTON__


#include <stdint.h>


class Button { 
public: 
  virtual int16_t get_status() = 0;
};


#endif
