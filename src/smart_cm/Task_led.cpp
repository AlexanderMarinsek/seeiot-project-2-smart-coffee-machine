#include "Task_led.h"


Task_led::Task_led (int16_t pin1, int16_t pin2, int16_t pin3, StateImp **state_pp){
    //locked = false;
    this->pin1 = pin1;
    this->pin2 = pin2;
    this->pin3 = pin3;
    pinMode(pin1, OUTPUT);
    pinMode(pin2, OUTPUT);
    pinMode(pin3, OUTPUT);
    /*digitalWrite(this->pin1, LOW);
    digitalWrite(this->pin2, LOW);
    digitalWrite(this->pin3, LOW);*/
    this->state_pp = state_pp;
    this->timer = new TimerImp(TIMER_LED_PERIOD);
}

Task_led::~Task_led (){
    delete this->timer;
    digitalWrite(this->pin1, LOW);
    digitalWrite(this->pin2, LOW);
    digitalWrite(this->pin3, LOW);
}

void Task_led::run() {
    
    if (this->timer->has_finished()) {
        delete this->timer;
        this->timer = new TimerImp(TIMER_LED_PERIOD);
        this->led_status = this->led_status + 1;
    }
    switch (this->led_status) {
        case 1:
            digitalWrite(this->pin1, HIGH);
            digitalWrite(this->pin2, LOW);
            digitalWrite(this->pin3, LOW);
            break;
        case 2:
            digitalWrite(this->pin1, HIGH);
            digitalWrite(this->pin2, HIGH);
            digitalWrite(this->pin3, LOW);
            break;
        case 3:
            digitalWrite(this->pin1, HIGH);
            digitalWrite(this->pin2, HIGH);
            digitalWrite(this->pin3, HIGH);
            break;
        default:
            digitalWrite(this->pin1, LOW);
            digitalWrite(this->pin2, LOW);
            digitalWrite(this->pin3, LOW);
    }
    //Serial.print("***Led status: ");
    //Serial.println(this->led_status);
}
