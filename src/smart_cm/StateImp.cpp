#include "StateImp.h"
#include "Arduino.h"

void StateImp::update_pir_val(bool val) {
  this->pir_val = val;
  Serial.print("Pir: ");
  Serial.println(val);
}

void StateImp::update_pot_val(int16_t val) {
  this->pot_val = val;
  Serial.print("Pot: ");
  Serial.println(val);
}

void StateImp::update_dist_val(int16_t val) {
  this->dist_val = val;
  Serial.print("Dist: ");
  Serial.println(val);
}

void StateImp::update_btn_val(int16_t val) {
  this->btn_val = val;
  Serial.print("Btn: ");
  Serial.println(val);
}

/*
void StateImp::update_timer_val(unsigned long val) {
  this->timer_val = val;
  Serial.print("Timer: ");
  Serial.println(val);
}
*/
