#ifndef __TASK_READ_BTN__
#define __TASK_READ_BTN__

#include "Task.h"
#include "ButtonImp.h"
#include "Pins.h"
#include <stdint.h>


class Task_read_btn: public Task {
private:
    int16_t pin;
    Button *button;
public:
    Task_read_btn(int16_t pin, StateImp **state_pp);
    ~Task_read_btn ();
    void run();
};


#endif
