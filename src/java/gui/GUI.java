package gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import seiot.classi.msg.jssc.*;
import seiot.classi.msg.CommChannel;


public class GUI {
	private static final int DEFAULT_COFFEE_LEVEL = 512;
	private static final int NUMBER_OF_COFFEES = 3;
	private static final String TITLE = "Coffee Machine";
	private static final String BUTTONSTRING = "REFILL!!";
    private static final int PROPORTION = 5;
    private final JFrame frame;
    private final JPanel canvas;
    private final JButton button;
    private final JPanel myPanel;
    private final JPanel myPanel2;
    private final JPanel myPanel3;

    private final JLabel label;
    private final JLabel sugar_label;
    private final JTextField text;
    private final JProgressBar progressBar;
    private Integer sugar_level;
    private Integer nCoffees;
    //private String msg;
    private String port;
	private CommChannel channel;		

    
    public GUI(String port) throws Exception{
    	this.port = port;
    	this.channel = new SerialCommChannel(this.port, 9600);
    	
    	sugar_level = DEFAULT_COFFEE_LEVEL;
    	nCoffees = NUMBER_OF_COFFEES;
    	this.frame = new JFrame(TITLE);
    	
        this.canvas = new JPanel();
        canvas.setLayout(new BorderLayout());     
        
        frame.setContentPane(this.canvas);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.myPanel = new JPanel();
        myPanel.setLayout(new BoxLayout(this.myPanel, BoxLayout.LINE_AXIS));

        this.myPanel2 = new JPanel();
        myPanel2.setLayout(new BoxLayout(this.myPanel2, BoxLayout.LINE_AXIS));
        
        this.myPanel3 = new JPanel();
        myPanel3.setLayout(new BoxLayout(this.myPanel3, BoxLayout.LINE_AXIS));
        
        this.progressBar = new JProgressBar();
        progressBar.setMaximum(1023);
        progressBar.setMinimum(0);
        progressBar.setVisible(true);
        progressBar.setValue(sugar_level);
        progressBar.setStringPainted(true);
        
        
        myPanel.add(progressBar, BoxLayout.X_AXIS);
        
        this.sugar_label = new JLabel("Sugar level: ");
        myPanel.add(sugar_label, BoxLayout.X_AXIS);
        
        this.label = new JLabel("");
        myPanel2.add(label, BoxLayout.X_AXIS);
        
        this.button = new JButton(BUTTONSTRING);
        button.setEnabled(false);
        myPanel3.add(button, BoxLayout.X_AXIS);
        
        this.text = new JTextField();
        text.setVisible(true);
        myPanel3.add(text, BoxLayout.X_AXIS);
        
        canvas.add(myPanel, BorderLayout.NORTH);
        canvas.add(myPanel2, BorderLayout.WEST);
        canvas.add(myPanel3, BorderLayout.AFTER_LAST_LINE);
        this.button.addActionListener(new ActionListener() {
         
            @Override
            public void actionPerformed(final ActionEvent e) {
            	//int nc = Integer.valueOf(text.getText());
            	int nc =  NUMBER_OF_COFFEES;
            	//text.setText("");
            	nCoffees += nc;
                label.setText("Machine refilled with " + nc + " coffees");
                button.setEnabled(false);
                String msg = "TT-Refill-" + nc;
//                System.out.println(msg);
                channel.sendMsg(msg);
            }
        });
        
        this.sugar_level = DEFAULT_COFFEE_LEVEL;
        progressBar.setString(getSugarLevel(this.sugar_level) + "%");
    }
    
    private int getSugarLevel(int value) {
    	return ((value*100)/1023);
    }
    
    public void display() {
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / PROPORTION, sh / PROPORTION);        
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

    
    
    
    public void launch() throws Exception {
    	
		// attesa necessaria per fare in modo che Arduino completi il reboot 
        label.setText("Waiting Arduino for rebooting...");		
		Thread.sleep(4000);
		label.setText("");	
		String msg = "";
		
		//this.channel.receiveMsg();
		while(true) {
		
			
			//msg = "TT-Empty";
			msg = channel.receiveMsg().trim();
			msg = msg.replaceAll("\n", "");
			System.out.println(msg);

			if(msg.contains("TT-zucchero-")) {
				String m = String.valueOf(msg.charAt(12)) + String.valueOf(msg.charAt(13)) + String.valueOf(msg.charAt(14)) + String.valueOf(msg.charAt(15));
				System.out.println(Integer.valueOf(m));
				this.sugar_level = Integer.valueOf(m);
				this.label.setText("Sugar level is now " + getSugarLevel(this.sugar_level) + "%" );
				this.progressBar.setValue(this.sugar_level);
		        progressBar.setString(getSugarLevel(this.sugar_level) + "%");
			} else {
				switch (msg){
				case "TT-Welcome":
					this.label.setText("Welcome");
					break;
				case "TT-Doing":
					this.label.setText("Making a coffee");
					this.nCoffees--;
					break;
				case "TT-Ready":
					this.label.setText("The coffee is ready");
					break;
				case "TT-Empty":
					this.button.setEnabled(true);
					this.label.setText("MAINTENANCE MODE : NEED REFILL");
				default:
					//System.out.println("......");
					break;
				}
			}	
		}
    }	
}
