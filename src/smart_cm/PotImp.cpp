#include "PotImp.h"
#include "Arduino.h"
#include <stdint.h>


PotImp::PotImp (int16_t outputPin) {
    this->outputPin = outputPin;
    pinMode(outputPin, INPUT);  
}


int16_t PotImp::get_value() {
    int16_t a = analogRead(this->outputPin);
    //a = (int16_t)(a/1023.0 * 3);
    return a;
}
