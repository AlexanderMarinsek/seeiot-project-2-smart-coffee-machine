#ifndef __TASKS_STATE_M__
#define __TASKS_STATE_M__

#include "Task.h"
#include "StateImp.h"
#include "Scheduler.h"
#include <stdint.h>


#define INITIAL_NUM_OF_COFFEE   3


class Task_state_m: public Task {
public:
    //Task_state_m(State* state);
    Task_state_m(StateImp **state_pp, Scheduler *sched_p);
    void run();
protected:
    Scheduler *sched_p;
};

#endif
