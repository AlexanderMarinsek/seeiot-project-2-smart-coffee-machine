#include "Task_read_btn.h"


Task_read_btn::Task_read_btn (int16_t pin, StateImp **state_pp){
    //locked = false;
    this->pin = pin;
    this->state_pp = state_pp;
    this->button = new ButtonImp(BTN_PIN);
    //this->button = new ButtonFake();
}

Task_read_btn::~Task_read_btn (){
    delete this->button;
}

void Task_read_btn::run() {
    (*this->state_pp)->update_btn_val(this->button->get_status());
}
