
#define LED_PIN_1       3
#define LED_PIN_2       4
#define LED_PIN_3       5

#define POT_PIN         A0
#define BTN_PIN      	6
#define PIR_PIN         2
#define ECHO_PIN        7
#define TRIG_PIN        8

#define WAKEUP_PIN      PIR_PIN
