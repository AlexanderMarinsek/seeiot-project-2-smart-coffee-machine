#ifndef __TASK_READ_POT__
#define __TASK_READ_POT__

#include "Task.h"
#include "StateImp.h"
#include "PotImp.h"
#include "Pins.h"
#include <stdint.h>


class Task_read_pot: public Task {
private:
    int16_t pin;
    Pot *pot;
public:
    Task_read_pot(int16_t pin, StateImp **state_pp);
    ~Task_read_pot ();
    void run();
};


#endif
