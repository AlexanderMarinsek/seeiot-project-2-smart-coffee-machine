#ifndef __TIMER__
#define __TIMER__


#include <stdint.h>


#define TIMER_DT1_MS      1000
#define TIMER_DT2A_MS     5000
#define TIMER_DT2B_MS     5000
#define TIMER_DT3_MS      3000
#define TIMER_DT4_MS      5000
#define TIMER_LED_PERIOD  TIMER_DT3_MS/3


class Timer {
public:
    virtual int16_t has_finished() = 0;
};


#endif
