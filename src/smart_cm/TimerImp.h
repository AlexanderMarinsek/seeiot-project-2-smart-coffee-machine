#ifndef __TIMERIMP__
#define __TIMERIMP__


#include "Timer.h"
#include <stdint.h>


class TimerImp: public Timer {
private:
    unsigned long period_ms;
    unsigned long start_time;
public:
    TimerImp(unsigned long period_ms);
    int16_t has_finished();
};


#endif
